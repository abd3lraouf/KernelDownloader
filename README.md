# latest Kernel Downloader and updater

This script does the following

- [x] Download latest {RC, Low latency or Release} kernel
- [x] Install kernel files


## Features
- [x] Saves the new kernel to external directory
- [x] Resuming download
- [ ] Downlaod specific version

## Usage

### using git

```bash
cd /tmp/
git clone https://gitlab.com/AbdElraoufSabri/KernelDownloader.git
cd KernelDownloader
chmod +x kernelupgrade.sh
./kernelupgrade.sh
```

### using wget
```bash
cd /tmp/
wget https://gitlab.com/AbdElraoufSabri/KernelDownloader/-/archive/master/KernelDownloader-master.zip
unzip KernelDownloader-master.zip
cd KernelDownloader-master/
chmod +x kernelupgrade.sh
./kernelupgrade.sh
```